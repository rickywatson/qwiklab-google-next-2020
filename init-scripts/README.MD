# Overview 

There are two scripts in this folder: 

* [`gitlab-init-gke.sh`](gitlab-init-gke.sh) : By executing this script, you will provision a 3 node GKE cluster and deploy GitLab on a version consistent with the latest [helm chart](https://gitlab.com/gitlab-org/charts/gitlab) currently available. All secrets and dependencies like GitLab Runners will also be deployed on the GKE cluster that gets provisioned. The only pre-requisite of executing this script is to have [gcloud](https://cloud.google.com/sdk/install) CLI/SDK installed and authenticated with a user account or service account with [`container.clusters.*`](https://cloud.google.com/iam/docs/understanding-roles) and [`compute.addresses.list.*`](https://cloud.google.com/iam/docs/understanding-roles) permissions at minimum.
  * This script takes **no input parameters**.
  * This script provides **verbose output** of various API calls and environment variables for troubleshooting. Some outputs include the `Root` user password along with the GitLab login page URL endpoint to access the UI
  * The GKE Cluster will be provisioned in the `us-central1` region by default in a random zone (`a`, `b`, `c`, or `f`)
  * To execute this script, simply enter the following command in the local directory: `./gitlab-init-gke.sh`

> This script can be run before lab provisioning in a clean new GCP Project by GCP project admin for demo purposes. **This script can take up to 8-12 minutes to complete, so be patient!**


* [`gitlab-config.sh`](./gitlab-config.sh) : By executing this script, you will provision 2 new users : `developer` and `operator` on GitLab. This script assumes you used the prior script to provision GitLab on GKE. The only pre-requisite of executing this script is to have [gcloud](https://cloud.google.com/sdk/install) CLI/SDK installed and authenticated with a user account or service account with [`compute.addresses.list.*`](https://cloud.google.com/iam/docs/understanding-roles) permissions at minimum.
  * This script takes **1 input parameter**, a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) of a [GitLab admin user](https://docs.gitlab.com/ee/user/permissions.html). This script also automatically licenses the GitLab instance with [GitLab Ultimate](https://about.gitlab.com/pricing/self-managed/feature-comparison/) for a max of 2 concurrent [regular users](https://docs.gitlab.com/ee/user/permissions.html). The only pre-requisite of executing this script is to have [gcloud](https://cloud.google.com/sdk/install) CLI/SDK installed and authenticated with a user account or service account with [`container.clusterAdmin`](https://cloud.google.com/iam/docs/understanding-roles) role at minimum.
  * This script provides **verbose outputs** containing the `username` and `password` of each of the two new users created. You can then use these credentials to log into GitLab as either of those users.
  * * To execute this script, simply enter the following command in the local directory: `./gitlab-confige.sh '<PERSONAL-ACCESS-TOKEN-STRING>'`

> Login to your GitLab instance based on the URL and root password provided as an output from the first script. Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and use that as the input to this script. 