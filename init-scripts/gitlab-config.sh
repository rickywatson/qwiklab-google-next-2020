#!/bin/bash

###############################
# To execute this script, you need an admin user's personal GitLab Token
# chmod +x ./gitlab-config.sh
# ./gitlab-config.sh <PERSONAL_ACCESS_TOKEN>
###############################

# Get the variables needed to make API calls against GitLab Instance
export TOKEN=$1
export LB_IP_ADDR="$(gcloud compute addresses list | awk '/endpoints-ip/ {print$2}')"
export GITLAB_URL="https://gitlab.$LB_IP_ADDR.nip.io"
export EMAIL_1="user1@test.com"
export USERNAME_1="Developer"
export PASSWORD_1="$(openssl rand -base64 8)"
export EMAIL_2="user2@test.com"
export USERNAME_2="Operator"
export PASSWORD_2="$(openssl rand -base64 8)"

# API Call to create the first user "Developer"
curl --header "Private-Token: $TOKEN" \
	 -d "name=$USERNAME_1&email=$EMAIL_1&username=$USERNAME_1&password=$PASSWORD_1" \
	 -X POST \
	 "$GITLAB_URL/api/v4/users" > /dev/null

# API Call to create the first user "Operator"
curl --header "Private-Token: $TOKEN" \
	 -d "name=$USERNAME_2&email=$EMAIL_2&username=$USERNAME_2&password=$PASSWORD_2" \
	 -X POST \
	 "$GITLAB_URL/api/v4/users" > /dev/null

# Print the credentials for the two users 
printf "\n\nUsername: $USERNAME_1\nPassword: $PASSWORD_1 \n\n" 
printf "\n\nUsername: $USERNAME_2\nPassword: $PASSWORD_2 \n\n" 

# Get the License Key 
export LICENSE_KEY="$(curl https://gitlab.com/gitlab-workshops/gitlab-on-gke-gnext/raw/master/gitlab-on-gke/gitlab.license)"

# Apply the license key to GitLab
curl --header "Private-Token: $TOKEN" \
	 -d "license=$LICENSE_KEY" \
	 -X POST \
	 "$GITLAB_URL/api/v4/license"