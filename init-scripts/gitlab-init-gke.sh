#!/bin/bash

#Configure an arbitrary zone
python -c 'import random, os; l = random.choice(["a", "b", "c", "f"]); os.system("gcloud config set compute/zone us-central1-{}".format(l)); print("You will be using GCP zone: us-central1-{}".format(l))'

# Provision a GKE Cluster
gcloud container clusters create gitlab-gke-cl --num-nodes=3 --machine-type=n1-standard-4 --disk-type "pd-ssd" --disk-size "100"

# Get context for kubectl
gcloud container clusters get-credentials gitlab-gke-cl

# Install Helm
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash

# Create Service Account for Helm 
kubectl create serviceaccount -n kube-system tiller

# Bind Role to service account
kubectl create clusterrolebinding tiller-binding --clusterrole=cluster-admin --serviceaccount kube-system:tiller

# Initialize Helm 
helm init --service-account tiller

# Add GitLab Helm Repo 
helm repo add gitlab https://charts.gitlab.io/

# Update Helm Repo 
helm repo update

# Allocate Static IP 
gcloud compute addresses create endpoints-ip --region us-central1

# Save Static IP as local environment variable
export LB_IP_ADDR="$(gcloud compute addresses list | awk '/endpoints-ip/ {print$2}')"; echo "LB_IP_ADDR=${LB_IP_ADDR}"

# Set email address for LE challenge for SSL cert
export EMAIL="$(gcloud auth list 2> /dev/null | awk '/^\*/ {print $2}')"; echo $EMAIL

# Create GitLab namespace
kubectl create namespace gitlab

echo "Waiting for DNS propagation and Tiller initialization."
sleep 4m

# Deploy GitLab to GKE Cluster
helm upgrade --install gitlab gitlab/gitlab                    \
             --timeout 600                                     \
             --namespace gitlab 							   \
             --set global.hosts.externalIP=${LB_IP_ADDR}       \
             --set global.hosts.domain=${LB_IP_ADDR}.nip.io    \
             --set gitlab-runner.runners.privileged=true       \
             --set certmanager-issuer.email=${EMAIL}

# Wait for unicorn app to be in READY state
echo "Waiting for Unicorn pods."
sleep 4m

# Login URL to GitLab 
export GITLAB_URL="https://gitlab.$LB_IP_ADDR.nip.io"
echo; echo $GITLAB_URL ; echo

# Default Root Password 
export ROOT_PASSWORD=$(kubectl get secret gitlab-gitlab-initial-root-password  -n gitlab -ojsonpath={.data.password} | base64 --decode)
echo; echo $ROOT_PASSWORD ; echo